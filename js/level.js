function level() {
	this.current = 0;
	this.levels = [
		// [x, y, width, height]
		[
			[60,220,50,50],
			[90,220,50,50],
			[300,400,150,50],
			[501,400,50,50],
			[601,400,50,50],
			[401,200,200,50],
			[401,300,200,50],
			[50,550,50,50]
		],
		[
			[60,500,50,50],
			[300,500,50,50]
		],
		[	
			[20,400,50,50]
		],
	];
	this.next = function() {
		if(this.current == this.levels.length) this.current = 0;
		
		var level = this.levels[this.current];
		this.current++;
		return level;
	}
	
}