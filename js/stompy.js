
$(document).foundation();
var canvas = document.getElementById("canvas");
var ctx;
var player;
var score = 0;
var highScore = 0;
var airborn;
//var shieldScore = 0;
var pointActive = false;
var jumpTimer = 0;
var shieldTimer = 0;
var coinSound = new Audio("sounds/coin.mp3");
coinSound.volume = 0.3;
var audio = new Audio('sounds/dontask.mp3');

var music = new Audio('sounds/music.mp3');
music.volume = 0.2;
var brick = new Image();
brick.src = "img/brick2.png";
var sprites = {
	left: new sprite(30,38,30,38),
	leftJump: new sprite(0,38,30,38),
	right: new sprite(0,0,30,38),
	rightJump: new sprite(30,0,30,38),
	shield: new sprite(60,3,73,83),
	coin: new sprite(3,81,64,64)
	
};
var targetCount = 0;
var facingRight = true;
var spritesheet = new Image();
var cowbellEnabled = false;
 var cowbell = new Audio('sounds/cowbell.mp3');
var cowbell2 = new Audio('sounds/cowbell.mp3');
var cowbell3 = new Audio('sounds/cowbell.mp3');
var cowbell4 = new Audio('sounds/cowbell.mp3');
var cowbell5 = new Audio('sounds/cowbell.mp3');
var cowbell6 = new Audio('sounds/cowbell.mp3');
var cowbell7 = new Audio('sounds/cowbell.mp3'); 
var error = new Audio('sounds/error.mp3');
var timestamp = Date.now();
var ACCEL = 300;
var MAX_VELOCITY = 400;

var MIN_VELOCITY = .5;
var JUMP_VELOCITY = 500;
var JUMP_TIME = .3;
var FRICTION_FACTOR = 3;
var DROP_FACTOR = 6;
var MAX_DELTA = 0.3;
var LETHAL_VELOCITY = 500;
var GRAVITY = 500;
var EDGE_CREEP = 7;
var SCORE_DIGITS = 6;
var SHIELD_TIME = 15;
var LEVEL_CHANGE = 5;
var pos = { 
	x: 0,
	y: 0,
	x2: 300,
	y2: 300,
	x3: 100,
	y3: 300,
	x4: 300,
	y4: 300,
	x5: 300,
	y5: 100,
};
var size = {
	w: 50,
	h: 50,
};



var pixel = 10;
var inputs = {
	left: false,
	up: false,
	right: false,
	down: false,
};
var level;
var platforms = [];
var target;
//var inputClicked = false;
//var ai = true;
var color = "white";
var color2 = "red";
var gameTime = 0;
var user;
function init() {
	user = prompt("What is your name?");
	checkCookie();
	spritesheet.src = 'img/okay.png';
	ctx = canvas.getContext('2d');
	
	canvas.width = 600;
	canvas.height = 600;	
	
	
	target = new entity(0, 0, 10, 10);
	shield = new entity(0, 0, 20, 20);
	player = new entity(0,canvas.height, 30, 38);
	
	level = new level();

	loadLevel(level.next());
//	shieldTimer = 5;
	
	reset();
	music.addEventListener('ended', function() {
		this.currentTime = 0;
		this.play();
	}, false);
	music.play();
	document.addEventListener("keydown", keyDown, false);
	document.addEventListener("keyup", keyUp, false);
	//document.addEventListener("mousedown", mouseDown, false)
	setTimeout(imgBack,15000);
	gameLoop();
	setInterval(function() { updateHigh(user) },15000);
	setInterval(changecolor,200);
	setTimeout(errorb,10000);
	setInterval(microsoft_move,1);
}
function loadLevel(list) {
	platforms = [];
	var platform;
	for(var p=0; p < list.length; p++) {
		platform = list[p];
		platforms.push(
			new entity(platform[0], platform[1], platform[2], platform[3])
		);
	}
	resetShield();
	moveTarget();
	shieldTimer = 0;
}
function placeItem(item) {
	while(true) {
	
		var platform = pick(platforms);
		item.setMidX(platform.getMidX());
		item.setMidY(platform.getTop() - platform.halfHeight);
		var success = true;
		for (var p=0; p<platforms.length; p++) {
			platform = platforms[p];
			if (collideRect(item, platform)) {
				success = false;
				break;
			}
		}
		if (success) break;
	}
}


function reset() {
	player.setBottom(canvas.height);
	player.setLeft(0);
	moveTarget();
	player.vx = 0;
	player.vy = 0;
}
function resetShield() {
	placeItem(shield);
}
function die() {
	score -= 5;
	resetShield();
}
function scorePoint() {
	coinSound.play();
	score += 5;
	//shieldScore += 1;
	targetCount++;
	if (score > highScore) highScore = score;
	/*if (shieldScore >= 5) {
		shieldTimer = 30;
		shieldScore = 0;
	}*/
	if (targetCount >= LEVEL_CHANGE) {
		loadLevel(level.next());
		targetCount = 0;
		reset();
	}
}
function moveTarget() {
	placeItem(target);
	
	
}
function imgBack() {
	document.getElementById("canvas").style.backgroundImage = "url('img/background.jpg')";
	 setTimeout(Math.floor(Math.random() * 15000));
}
function errorb() {
	//error.play();
	document.getElementById("canvas").style.backgroundImage = "url('img/nope.png')";
	setTimeout(imgBack,10000);
}
function changecolor() {
	color = chance.color({format: 'hex'});

}
function backchange() {
	document.getElementById("canvas").style.backgroundColor = chance.color({format: 'hex'});
}

function gameLoop() {
	updatePosition();
	handleCollison();
	updateCanvas();
	var now = Date.now();
	var delta = (now - timestamp) / 1000;
	if (delta > MAX_DELTA) delta = MAX_DELTA;
	gameTime += delta;
	//microsoft();
	window.requestAnimationFrame(gameLoop);
}
function handleCollison() { 
	airborn = true;
	var platform, dx, dy;
	for (var p=0; p<platforms.length; p++) {
		platform = platforms[p];
		if (collideRect(player, platform)) {
			dx = (platform.getMidX() - player.getMidX()) / platform.width;
			dy = (platform.getMidY() - player.getMidY()) / platform.height;
			
			//Horizontal bounce
			if (Math.abs(dx) > Math.abs(dy)) {
			
				
				if (dx < 0) {
					if (player.vx < 0) player.vx = 0;
					
					if (cowbellEnabled == true) cowbell5.play();
					player.setLeft(platform.getRight());
					player.vx = 0;
				}else{
					if (player.vx > 0) player.vx = 0;
					if (cowbellEnabled == true) cowbell6.play();
					player.setRight(platform.getLeft());
					player.vx = 0;
				}
			}else{ //vertical
			
				if (dy < 0) {
	
						if (player.vy < 0)  player.vy = 0;
					
						if (cowbellEnabled == true) cowbell7.play();
						player.setTop(platform.getBottom());
						player.vy = 0;
				}else{
					if (player.vy  > LETHAL_VELOCITY && shieldTimer <= 0) { reset(); die(); }else{
						if (player.vy > 0)  player.vy = 0;
						if (cowbellEnabled == true) cowbell.play();
						player.setBottom(platform.getTop());
						player.vy = 0;
						if(Math.abs(player.vx) < EDGE_CREEP) {
							var x = player.getMidX();
							if (x < platform.getLeft() && !inputs.right) {
								player.vx = -EDGE_CREEP;
							}else if (x > platform.getRight() && !inputs.left) {
								player.vx = EDGE_CREEP;
							}
						}
						airborn = false;
					}
					//player.vx = 0;
					
				}
			}
		}
	}
	
	if (collideRect(player,target))  { moveTarget(); scorePoint(); };
	if(collideRect(player, shield)) {
		shieldTimer = SHIELD_TIME;
		shield.setRight(-100);
		shield.setBottom(-100);
		
	}
	if (player.getLeft() < 0) {
		player.setLeft(0);
		player.vx = 0;
		if (cowbellEnabled == true) cowbell2.play();
	}else if (player.getRight() > canvas.width) {
		//player.vx *= -1;
		player.vx = 0;
		player.setRight(canvas.width);
		if (cowbellEnabled == true) cowbell3.play();
	}
	if (player.getTop() < 0) {
		player.setTop(0);
		player.vy = 0;
		if (cowbellEnabled == true) cowbell4.play();
		//player.setBottom(canvas.height);
	}else if (player.getBottom() > canvas.height) {
		if (cowbellEnabled == true) cowbell.play();
		if(player.vy > LETHAL_VELOCITY && shieldTimer <= 0) { reset();  die();}
		else{
			player.setBottom(canvas.height);
			//player.setTop(0);
			if (cowbell == true) cowbell2.play();
			player.vy = 0;
		airborn = false;
		}
	}
}

function microsoft() {
	ctx.fillStyle = "red";
	ctx.fillRect(pos.x2, pos.y2, 50,50);
	ctx.fillStyle = "green";
	ctx.fillRect(pos.x2+50, pos.y2, 50,50);
	//
	ctx.fillStyle = "blue";
	ctx.fillRect(pos.x2, pos.y2+50, 50,50);
	ctx.fillStyle = "yellow";
	ctx.fillRect(pos.x2+50, pos.y2+50, 50,50);
	
	ctx.fillStyle = "red";
	ctx.fillRect(pos.x3, pos.y3, 50,50);
	ctx.fillStyle = "green";
	ctx.fillRect(pos.x3+50, pos.y3, 50,50);
	//
	ctx.fillStyle = "blue";
	ctx.fillRect(pos.x3, pos.y3+50, 50,50);
	ctx.fillStyle = "yellow";
	ctx.fillRect(pos.x3+50, pos.y3+50, 50,50);
	
	
	ctx.fillStyle = "red";
	ctx.fillRect(pos.x4, pos.y4, 50,50);
	ctx.fillStyle = "green";
	ctx.fillRect(pos.x4+50, pos.y4, 50,50);
	//
	ctx.fillStyle = "blue";
	ctx.fillRect(pos.x4, pos.y4+50, 50,50);
	ctx.fillStyle = "yellow";
	ctx.fillRect(pos.x4+50, pos.y4+50, 50,50);
	
	ctx.fillStyle = "red";
	ctx.fillRect(pos.x5, pos.y5, 50,50);
	ctx.fillStyle = "green";
	ctx.fillRect(pos.x5+50, pos.y5, 50,50);
	//
	ctx.fillStyle = "blue";
	ctx.fillRect(pos.x5, pos.y5+50, 50,50);
	ctx.fillStyle = "yellow";
	ctx.fillRect(pos.x5+50, pos.y5+50, 50,50);
	
	//microsoft_move();
}
function microsoft_move() {
	pos.x2 += Math.floor(Math.random() * 50) - 25;
	pos.y2 += Math.floor(Math.random() * 50) - 25;
	if (pos.x2 < 0) {
		//player.x = 0;
		//error.play();
		pos.x2 = canvas.width - size.w;
	}else if (pos.x2 > canvas.width - size.w) {
		//player.x = canvas.width - size.w;
		pos.x2 = 0;
		//error.play();
	}
	if (pos.y2 < 0) {
		//player.y = 0;
		//error.play();
		pos.y2 = canvas.height - size.h;
	}else if (pos.y2 > canvas.height - size.h) {
		//player.y = canvas.height - size.h;
		pos.y2 = 0;
		//error.play();
	}
	
	pos.x3 += Math.floor(Math.random() * 50) - 25;
	pos.y3 += Math.floor(Math.random() * 7) - 3;
	if (pos.x3 < 0) {
		//player.x = 0;
		//error.play();
		pos.x3 = canvas.width - size.w;
	}else if (pos.x3 > canvas.width - size.w) {
		//player.x = canvas.width - size.w;
		pos.x3 = 0;
		//error.play();
	}
	if (pos.y3 < 0) {
		//player.y = 0;
		//error.play();
		pos.y3 = canvas.height - size.h;
	}else if (pos.y3 > canvas.height - size.h) {
		//player.y = canvas.height - size.h;
		pos.y3 = 0;
		//error.play();
	}
	
	pos.x4 += Math.floor(Math.random() * 50) - 25;
	pos.y4 += Math.floor(Math.random() * 50) - 25;
	if (pos.x4 < 0) {
		//player.x = 0;
		//error.play();
		pos.x4 = canvas.width - size.w;
	}else if (pos.x4 > canvas.width - size.w) {
		//player.x = canvas.width - size.w;
		pos.x4 = 0;
		//error.play();
	}
	if (pos.y4 < 0) {
		//player.y = 0;
		//error.play();
		pos.y4 = canvas.height - size.h;
	}else if (pos.y4 > canvas.height - size.h) {
		//player.y = canvas.height - size.h;
		pos.y4 = 0;
		//error.play();
	}
	
	pos.x5 += Math.floor(Math.random() * 7) - 3;
	pos.y5 += Math.floor(Math.random() * 7) - 3;
	if (pos.x5 < 0) {
		//player.x = 0;
		//error.play();
		pos.x5 = canvas.width - size.w;
	}else if (pos.x5 > canvas.width - size.w) {
		//player.x = canvas.width - size.w;
		pos.x5 = 0;
		//error.play();
	}
	if (pos.y5 < 0) {
		//player.y = 0;
		//error.play();
		pos.y5 = canvas.height - size.h;
	}else if (pos.y5 > canvas.height - size.h) {
		//player.y = canvas.height - size.h;
		pos.y5 = 0;
		//error.play();
	}
}
function updatePosition() {
	var now = Date.now();
	var delta = (now - timestamp) / 1000;
	if (delta > MAX_DELTA) delta = MAX_DELTA;
	timestamp = now;
	//console.log(delta);
	if (inputs.up) {
		if (!airborn) {
			jumpTimer = JUMP_TIME;
			player.vy = -JUMP_VELOCITY
			
		}
		if(jumpTimer > 0) {
			jumpTimer -= delta;
		}else{
			if (jumpTimer) jumpTimer = 0;
			if(player.vy < 0) player.vy -= delta * player.vy * DROP_FACTOR;
			player.vy += delta * GRAVITY;
		}
		//player.vy -= delta * ACCEL;
		
		
	}else if (inputs.down) {
		player.vy += delta * ACCEL;

	}else{
		//player.vy -= delta * player.vy * FRICTION_FACTOR;
		player.vy += delta  * GRAVITY;
	}
	
	if (inputs.left) {
		facingRight = false;
		if(!airborn && player.vx > 0) {
			player.vx -= delta * player.vx * FRICTION_FACTOR;
		}
			player.vx -= delta * ACCEL;

	}else if (inputs.right) {
		facingRight = true;
		if(!airborn && player.vx < 0) {
			player.vx -= delta * player.vx * FRICTION_FACTOR;
		}
			player.vx += delta * ACCEL;

	}else if(!airborn){
		player.vx -= delta * player.vx * FRICTION_FACTOR;
	}
	
	
	if (player.vx > MAX_VELOCITY) {
		player.vx = MAX_VELOCITY;
	}else if (player.vx < -MAX_VELOCITY) {
		player.vx = -MAX_VELOCITY;
	}else if (Math.abs(player.vx) < MIN_VELOCITY) {
		player.vx = 0;
	}
	
	
	
	player.x += delta * player.vx;
	player.y += delta * player.vy;

	if(shieldTimer > 0) {
		shieldTimer -= delta;
		if (shieldTimer < 0) {
			shieldTimer = 0;
			resetShield();
		}
	}
	

}
function updateCanvas() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	
	var sprite;
	ctx.font = '12px Arial';
	ctx.fillStyle = "gold";
	ctx.fillText("High: " + pad(highScore,SCORE_DIGITS),20,40);
	ctx.fillStyle = "gold";
	ctx.fillText("Score: " + pad(score,SCORE_DIGITS),20,70);
	ctx.fillStyle = "white";
	if (updateNotice == true) {
		ctx.fillStyle = "black";
		ctx.font = '12px Arial';
		ctx.fillText("Updating high score",250,250);
		}
	var time = gameTime * 100;
	time = Math.round(time * 100) / 100;
	ctx.fillText(time,520,40); ctx.fillStyle = "gold"
	if (shieldTimer > 0) {
		/*
		ctx.fillText(Math.ceil(shieldTimer), 580,40);*/
		
		var bar = shieldTimer / SHIELD_TIME * player.width;
		barColor = "green";
		if(shieldTimer < 5){
			barColor = "red";
		}else if(shieldTimer < 10) {
			barColor = "yellow";
		}
		ctx.fillStyle = barColor;
		ctx.fillRect(player.getLeft(),player.getTop() -10, bar, 4);
	}
	
	if(airborn) { 
		if(facingRight) {
			sprite = sprites.rightJump;
		}else{
			sprite = sprites.leftJump;
		}
	}else{
		if (facingRight) {
			sprite = sprites.right;
		}else{
			sprite = sprites.left;
		}
	}
	drawSprite(sprite, player);
	//ctx.fillStyle = color;
	//ctx.fillRect(player.x, player.y, player.width, player.height);
	//ctx.fillStyle = color;
		drawSprite(sprites.shield,shield);
		//ctx.fillRect(shield.x, shield.y, shield.width, shield.height);
		//ctx.fillStyle = 'gold';
		drawSprite(sprites.coin,target);
		//ctx.fillRect(target.x, target.y, target.width, target.height);
		
	for (var i=0; i < platforms.length; i++) {
		ctx.drawImage(brick,platforms[i].x,platforms[i].y,platforms[i].width,platforms[i].height);
		//ctx.fillRect(platforms[i].x, platforms[i].y, platforms[i].width, platforms[i].height);
	}
	
	
	/*ctx.fillStyle = color2;
	ctx.fillRect(pos2.x, pos2.y, size.w2,size.h2);*/
}
function keyDown(e) {
	e.preventDefault();
	
	//console.log(e.keyCode);
	switch(e.keyCode) {
		case 82:
			moveTarget();
			break;
		case 191: 
			player.vy -=  player.vy * FRICTION_FACTOR;
			player.vx -= player.vx * FRICTION_FACTOR;
			break;
		case 72:
			music.play()
			break;
		case 71:
			audio.play()
			break;
		case 69:
			error.play()
			break;
		case 32: 
			inputs.up = true;
			break;
		case 87: //up (W)
			//pos.y -= pixel
			inputs.up = true;
			break;
		/*case 83: //down (S)
			inputs.down = true;
			break;*/
		case 65: //left (A)
			inputs.left = true;
			break;
			
		case 68: //RIGHT (D)
			inputs.right = true;
			break;
			
		//
		case 38: //up 
			inputs.up = true;
			break;
		/*case 40: //down 
			inputs.down = true;
			break;*/
		case 37: //left 
			inputs.left = true;
			break;
		case 39: //RIGHT 
			inputs.right = true;
			break;

	}

}
function keyUp(e) {
	e.preventDefault();
	//inputClicked = false;
	switch(e.keyCode) {
		case 32:
			inputs.up = false;
			break;
		case 87: //up (W)
			//pos.y -= pixel
			inputs.up = false;
			break;
		/*case 83: //down (S)
			inputs.down = false;
			break;*/
		case 65: //left (A)
			inputs.left = false;
			break;
			
		case 68: //RIGHT (D)
			inputs.right = false;
			break;
			
		//
		case 38: //up 
			inputs.up = false;
			break;
		/*case 40: //down 
			inputs.down = false;
			break;*/
		case 37: //left 
			inputs.left = false;
			break;
		case 39: //RIGHT 
			inputs.right = false;
			break;
	}
}
function mouseDown(e) {
	
	var rect = canvas.getBoundingClientRect();
	var x = e.clientX - rect.left;
	var y = e.clientY - rect.top;
	
	player.setMidX(x);
	player.setMidY(y);
}
function collideRect(a, b) {
	if (a.getLeft() > b.getRight()) return false;
	if (a.getTop() > b.getBottom()) return false;
	if (a.getRight() < b.getLeft()) return false;
	if (a.getBottom() < b.getTop()) return false;
	
	return true;
}
function drawSprite(s,e) {
	ctx.drawImage(spritesheet, s.x,s.y,s.width,s.height, e.x, e.y, e.width, e.height);
}
function pick(list) {
	var index = Math.floor(Math.random() * list.length);
	return list[index];
}